var class_u_g_b_loca_parser =
[
    [ "CLocaEntry", "class_u_g_b_loca_parser_1_1_c_loca_entry.html", "class_u_g_b_loca_parser_1_1_c_loca_entry" ],
    [ "UGBLocaParser", "class_u_g_b_loca_parser.html#a5080e893065103c9a2d17ac04088e6ca", null ],
    [ "Clear", "class_u_g_b_loca_parser.html#a900b151894fb659a8a3ef83a8e8fc68a", null ],
    [ "GetEntries", "class_u_g_b_loca_parser.html#a9cdef5b1e9ea9e696a07b1d26896ed29", null ],
    [ "GetLanguages", "class_u_g_b_loca_parser.html#af96fd86d89414870da521c39549b9ddd", null ],
    [ "Parse", "class_u_g_b_loca_parser.html#a647bce7cd0a7ce55bd7858f87e673f81", null ]
];