var class_game_options =
[
    [ "GetQuality", "class_game_options.html#ad24b4298bd78025a2c7d04139c076ebb", null ],
    [ "OnOptionChange", "class_game_options.html#a186ce5fb19d5947b5e3acc703b0404ee", null ],
    [ "SetQuality", "class_game_options.html#ae31006455eccfbb523c4530b8ae4843c", null ],
    [ "UpdateValues", "class_game_options.html#a334cd7e4be289994713fbe5e46664c76", null ],
    [ "kOptLanguage", "class_game_options.html#a65c95fcd82f7c72b739866d4c8789b24", null ],
    [ "isMusicOn", "class_game_options.html#a41ad23ca9fe469a19dd83b9bcb32a359", null ],
    [ "isOptionsDialogVisible", "class_game_options.html#ac377c197aa1e2ed369dbe5cb19163103", null ],
    [ "isSoundOn", "class_game_options.html#ac0eb2319521ef2f32f2ae91202c00ad5", null ],
    [ "language", "class_game_options.html#aadd7de7c9c7f21405e8f0d3ed50bc64c", null ],
    [ "nextLanguage", "class_game_options.html#a016613738f3ced3781539a65ad07a0b5", null ],
    [ "nextQuality", "class_game_options.html#a218482fc6f33a2d7f6da8b4007577ea1", null ],
    [ "showTouchFeedBack", "class_game_options.html#ae5ac8071cae78be77c08044e79611389", null ],
    [ "OnAnyOptionChanged", "class_game_options.html#afb2240304800657a2cc5fcad09f73db4", null ],
    [ "OnOptionDialogToggled", "class_game_options.html#a69af9b197e2a637478ff0e10cb010826", null ]
];