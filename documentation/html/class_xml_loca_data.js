var class_xml_loca_data =
[
    [ "XmlLocaDataEntry", "class_xml_loca_data_1_1_xml_loca_data_entry.html", "class_xml_loca_data_1_1_xml_loca_data_entry" ],
    [ "XmlLocaData", "class_xml_loca_data.html#a620270ce07d05b6b25c4b11bc9109ae8", null ],
    [ "PostRead", "class_xml_loca_data.html#ab38b57c97e75894632587dc0baded787", null ],
    [ "PreWrite", "class_xml_loca_data.html#ab155e06539f82a02868d1c2c3f1bd3b3", null ],
    [ "mData", "class_xml_loca_data.html#ab21c64c0490d07ead764d55622df4ffc", null ],
    [ "mDataBuffer", "class_xml_loca_data.html#a6fb861989629192f8efbb73e22ecc57e", null ],
    [ "mLanguage", "class_xml_loca_data.html#a642ee8837dfbfdb66ded88ff788c5cd3", null ]
];