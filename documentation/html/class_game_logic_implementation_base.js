var class_game_logic_implementation_base =
[
    [ "GameLogicImplementationBase", "class_game_logic_implementation_base.html#a9369452b50a5f314c401d05e77b279d6", null ],
    [ "GameSetupReady", "class_game_logic_implementation_base.html#a5684dffb148790081f5a07818d0e5a80", null ],
    [ "GameStateChanged", "class_game_logic_implementation_base.html#aa1458f51eb587f6008dfe61ee973128b", null ],
    [ "GetCurrentGameState", "class_game_logic_implementation_base.html#aab5de86a0e1e55d03342d58956d0133d", null ],
    [ "OnBeforePause", "class_game_logic_implementation_base.html#a48ea5eadc04597cdc1ebc7a5cd5b379e", null ],
    [ "OnBeforeRestart", "class_game_logic_implementation_base.html#a68030735c9185c2676b4697971cf6a93", null ],
    [ "OnFirstTimeSaveGameLoaded", "class_game_logic_implementation_base.html#ae4fa348a70cdca5abd724af4501e84d6", null ],
    [ "OnSaveGameLoaded", "class_game_logic_implementation_base.html#ae3be3c6eaabe363811541ef765233912", null ],
    [ "Start", "class_game_logic_implementation_base.html#a422c800d2309553cae2bee285b0987c2", null ],
    [ "UpgradeSaveGameEntry", "class_game_logic_implementation_base.html#a326ef5e53f99c0b11563ceacc75bd8b7", null ],
    [ "GData", "class_game_logic_implementation_base.html#a50c021b8a8e2c55c420dc7542474761d", null ],
    [ "GInput", "class_game_logic_implementation_base.html#a291649250693f87c867b8e9e5ffd1424", null ],
    [ "GLicense", "class_game_logic_implementation_base.html#a832b51d4ffca2597dec5c4d149a6d7a0", null ],
    [ "GLoca", "class_game_logic_implementation_base.html#a16197c0b3548df567f664e897117b956", null ],
    [ "GLogic", "class_game_logic_implementation_base.html#af970be0c19b55789a78bc95a632e225c", null ],
    [ "GMusic", "class_game_logic_implementation_base.html#a2bb2b81020c4074f6de3cd6c4a8f940e", null ],
    [ "GOptions", "class_game_logic_implementation_base.html#a71233a65d0cdb5a793c4b855afd85fa5", null ],
    [ "GPause", "class_game_logic_implementation_base.html#ad54bac87d5dbf7d365073833af1d2e62", null ],
    [ "GPlayer", "class_game_logic_implementation_base.html#afc3aaf86c92dfaf93552ca8a04229224", null ],
    [ "GSaveGames", "class_game_logic_implementation_base.html#a771cbcb9b7cea2715ce30d9cb7679eb2", null ],
    [ "GState", "class_game_logic_implementation_base.html#ad9a835b5dbc7153f13729122b6c0ead8", null ]
];