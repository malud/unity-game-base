using UnityEngine;
using System.Collections;

/// <summary>
/// Fades and Plays Music (if enabled) according to current game state. 
/// </summary>
public class GameMusic : GameComponent
{

	public bool mEnabled = true;
	
	
	public SMusicState currentState
	{
		get;
		private set;
	}
	
	public float mFadeTime = 0.5f;
	public float mVolume;


	int mCurrentActiveChannel;
	AudioClip mCurrentActiveClip;
	AudioSource[] mChannels;
	float mCalculatedVolume;
	bool mInitialized = false;
	
	
	
	
	void Start()
	{
		currentState = SMusicState.invalid;

		if(mInitialized)
			return;
#if UNITY_ANDROID
		mChannels = new AudioSource[1];
#else
		mChannels = new AudioSource[3];
#endif
		for(int i = 0;i<mChannels.Length;i++)
		{
			
			AudioSource s = this.gameObject.AddComponent<AudioSource>();
			s.playOnAwake = false;
			s.loop = false;
			s.dopplerLevel = 0;
			mChannels[i] = s;
		}
	
		mInitialized = true;
		GOptions.OnAnyOptionChanged += OnAnyOptionChangedEvent;
		
	}
	protected override void OnDestroy()
	{
		base.OnDestroy();
		GOptions.OnAnyOptionChanged -= OnAnyOptionChangedEvent;
		
	}
	
	
	public void OnAnyOptionChangedEvent()
	{
		if(mEnabled != GOptions.isMusicOn)
		{
			mEnabled = GOptions.isMusicOn;
			if(GOptions.isMusicOn)
			{
                mVolume = 1;
				//PlayClip(GetClipForState(currentState));
			}else
			{
                mVolume = 0;
				//StopAllClips(false);
			}
		}
	}
	
	
	
	
	public void StopAllClips(bool pImmediately)
	{
		if(pImmediately)
		{
			for (int i = 0; i < mChannels.Length; i++) 
			{
				if(!mChannels[i].isPlaying)
					continue;
				mChannels[i].Stop();
			}
		}else
		{
            mCurrentActiveClip = null;
			mCurrentActiveChannel = -1;
		}
	}
	
	
	void Update()
	{
		mEnabled = GOptions.isMusicOn;
		
		for (int i = 0; i < mChannels.Length; i++) 
		{

			if(i == mCurrentActiveChannel)
			{
				mChannels[i].volume = Mathf.Lerp(mChannels[i].volume,mVolume,Time.deltaTime / mFadeTime);
				if(!mChannels[i].isPlaying)
				{
					//
					// FIXME Somehow unity stops audio playback before anything else notices that unity is in background. 
					// 
					//
					mCurrentActiveChannel = -1;
					PlayClip(currentState);
					break;
				}
			}else
			{
				if(!mChannels[i].isPlaying)
					continue;

				mChannels[i].volume = Mathf.Lerp(mChannels[i].volume,0,Time.deltaTime / mFadeTime);
				if(mChannels[i].volume <= 0.02f)
				{
					mChannels[i].volume = 0;
					mChannels[i].Stop();

					if(mCurrentActiveChannel != -1 &&  mChannels[i].clip != mChannels[mCurrentActiveChannel].clip)
					{
						Resources.UnloadAsset( mChannels[i].clip );
					}

				}
			}
		}
	}
	
	public void PlayClip(SMusicState pState)
	{
		
		Debug.Log("GameMusic: " + pState);
		
		AudioClip _requestedClip  = GetClipForState(pState);
		PlayClip(_requestedClip);
		if(mCurrentActiveClip == _requestedClip)
		{
			Debug.Log("Clip playing:" + _requestedClip);
			currentState = pState;
		}else
		{
			Debug.Log("Not Playing Clip");
		}
	}
	
	void PlayClip(AudioClip pClip)
	{
		if(pClip == mCurrentActiveClip)
			return;
		
		if(pClip == null)
			return;
		
		mCurrentActiveClip = pClip;
		
		if(!mEnabled)
			return;
		
		
		 
		int freeIdx = GetFreeChannelIdx();
		if(freeIdx > mChannels.Length)
			freeIdx = mChannels.Length -1;
		
	
		
		mChannels[freeIdx].clip = pClip;
		mCurrentActiveChannel = freeIdx;
		mChannels[freeIdx].Play();
	}
	
	
	int GetFreeChannelIdx()
	{
		int i = 0;
		int minIdx = 0;
		bool found = false;
		float minVal = 1.0f;
		if(!mInitialized)
			Start();
		
		for (; i < mChannels.Length; i++) 
		{
			if(i != mCurrentActiveChannel)
			{
				if(!mChannels[i].isPlaying)
				{
					found = true;
					break;
				}
				
				if(mChannels[i].volume < minVal)
				{
					minVal = mChannels[i].volume;
					minIdx = i;
				}
			}
			
		}
		if(!found)
			return minIdx;
		return i;
	}
	
	AudioClip GetClipForState(SMusicState pState)
	{
		Debug.Log("Music requested: " + pState);
		AudioClip ac = pState.GetNextClip();
		return ac;
	}
}

