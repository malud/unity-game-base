﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace UGB.Test
{
	
	public class CGameMusicTest : GameComponent
	{
		GameMusic mMusic;
		List<SMusicState> mStates;

		void Awake ()
		{
			mMusic = gameObject.GetComponent<GameMusic>();
			mMusic.mVolume = 1.0f;

			mStates = new List<SMusicState>();

			SMusicState testStateA = SMusicState.Add(1, "TestStateA");
			testStateA.AddClip("MusicTestA");
			mStates.Add(testStateA);

			SMusicState testStateB = SMusicState.Add(2, "TestStateB");
			testStateB.AddClip("MusicTestB");
			mStates.Add(testStateB);

			SMusicState testStateC = SMusicState.Add(3, "TestStateC");
			testStateC.AddClip("MusicTestA");
			mStates.Add(testStateC);
		}


		public void Start ()
		{
			mMusic.PlayClip(1);

			StartCoroutine(ChangeMusicState(8, () => mMusic.PlayClip(2) ));
			StartCoroutine(ChangeMusicState(14, () => mMusic.PlayClip(3) ));
		}

		public IEnumerator ChangeMusicState (int Seconds, Action Callback) 
		{
			yield return new WaitForSeconds(Seconds);

			if(Callback != null)
				Callback();
		}

		protected override void OnDestroy()
		{
			foreach(var state in mStates)
				state.Unload();
		}

	}
}
